# YTDL

Small wrapper for [youtube-dl](https://github.com/ytdl-org/youtube-dl)

## Install:
- python 3.7
- pip install youtube-dl
- unzip ffmpegWin.zip and place the three executables somewhere on path (or download&install ffmpeg regularly)

## Use:
python ytdl.py  (optional comma-separated options) [comma-separated youtube links]
(without whitespace)

downloads are placed in a new generated output folder with structure output/(video-creator)/(video-title).(ext)

option "dump" just puts everything into output folder.

## Current options:
- no options -> downloads best audio quality and converts to mp3

- dump -> all files into output folder with no further structure
- playlist -> put in folder named after playlistName_playlistAuthor

- video -> downloads video instead of audio, default best available
- 480p, 720p, 1080p -> if opt "video" is set, filter for this resolution, optional

(this list might grow if other stuff needed)

## Example:

python ytdl.py video,720p,playlist https://www.youtube.com/watch?v=7u3jv7zC4kU&list=PLGdEbnOoiEOOaFFYKh3A66wOUlrHUwzTs

Downloads yt playlist into playlist folder in 720p (if available, or lower)

python ytdl.py https://www.youtube.com/watch?v=jZPMtyvL0fw,https://www.youtube.com/watch?v=sR8c3ugaSKM

Downloads mp3 of both youtube videos.