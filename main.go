package main

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/atotto/clipboard"
	"os"
	"os/exec"
	"strings"
	"time"
)

const parallel = 3
const delay = 200 * time.Millisecond
const mp3Mode = true

func main() {

	clip, err := clipboard.ReadAll()

	if err != nil {
		panic(err)
	}

	clip = sanitizeString(clip)
	links := strings.Split(clip, "\n")

	for i:=0; i<len(links); i++ {
		li := sanitizeString(links[i])
		links[i] = strings.TrimSuffix(li, ",")
	}

	finished := make(chan bool, len(links))
	pool := make(chan bool, parallel)

	for i:=0; i<parallel; i++ {
		pool <- true
	}

	for i,link := range links {
		if !strings.Contains(clip, "http") {
			fmt.Println("Link ",i," is no correct url.[",link,"]")
			finished <- true
			continue
		}

		time.Sleep(delay)
		<-pool
		go func(li string, n int) {
			time.Sleep(time.Second)
			err = downloadLink(li)
			if err != nil {
				fmt.Println("Link ",n," failed.[",li,"]")
			}
			finished <- true
			pool <- true
		}(link, i)

	}

	for i:=0; i<len(links); i++ {
		<-finished
	}

	fmt.Print("Press 'Enter' to exit...")
	bufio.NewReader(os.Stdin).ReadBytes('\n')

}

func downloadLink(link string) error {

	commands := []string{"ytdl.py"}
	if !mp3Mode {
		commands = append(commands, "video")
	}
	commands = append(commands, link)


	c := exec.Command("python", commands...)

	outP, err := c.Output()
	if err != nil {
		return err
	}

	if strings.Index(string(outP), "unable to download video") != -1 {
		return errors.New("unable to download video")
	}

	fmt.Println(string(outP))
	return nil
}

func sanitizeString(s string) string {
	s = strings.Trim(s, " ")
	s = strings.TrimSuffix(s, "\n")
	s = strings.TrimPrefix(s, "\n")
	s = strings.TrimSuffix(s, "\r")
	s = strings.TrimPrefix(s, "\r")
	s = strings.TrimSuffix(s, "\t")
	return strings.TrimPrefix(s, "\t")
}