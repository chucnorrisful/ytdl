from __future__ import unicode_literals
import youtube_dl
import sys
from pathlib import Path


class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')

def main(opts, urls):

    if 'video' in opts:
        print('VIDEO/MP4')
    else:
        print('AUDIO/MP3')

    outtmpl = './output/%(uploader)s/%(title)s.%(ext)s'
    if 'playlist' in opts:
        outtmpl = './output/%(playlist)s_%(playlist_uploader)s/%(title)s.%(ext)s'
    if 'dump' in opts:
        outtmpl = './output/%(title)s.%(ext)s'

    fmt = 'bestaudio/best'
    if 'video' in opts:
        fmt = 'best[ext=mp4]/best'
    if '480p' in opts:
        fmt = 'best[ext=mp4][height<=480]/best[height<=480]'
    if '720p' in opts:
        fmt = 'best[ext=mp4][height<=720]/best[height<=720]'
    if '1080p' in opts:
        fmt = 'best[ext=mp4][height<=1080]/best[height<=1080]'
    if '4k' in opts:
        fmt = 'bestvideo+bestaudio'
    
    ydl_opts = {
        'format': fmt,
        'outtmpl': outtmpl,
        'postprocessors': [],
        'logger': MyLogger(),
        'progress_hooks': [my_hook],
    }

    if 'video' in opts:
        ydl_opts['keepvideo'] = 'True'

    if not 'video' in opts:
        ydl_opts['postprocessors'] = [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }]

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download(urls)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("\n")
        print("[ERROR NO URL(s) SUBMITTED]\n")
        print("ytdl.py <opt1,opt2,opt3> url1,url2,url3")
        print("\n")
        exit()

    #if one argument: only video links, defaults to only mp3
    #if two arguments: first is options, second is video-links
    main(sys.argv[1].split(',') if len(sys.argv) > 2 else [], sys.argv[2].split(',') if len(sys.argv) > 2 else sys.argv[1].split(','))